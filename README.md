## Algebra I & II

***Note: I haven't got round to writing up and uploading all of my notes. This
should happen soon enough.***

*I have finally merged* `draft` *and* `master` *as* `master` *was lagging behind
so much.*

***UPDATE: The Algebra I summary should be complete with no errors. If you find
any, please let me know, or send a PR. The Algebra II notes however are still
under heavy development, with some very obvious errors and incomplete
statements. I should have this mainly fixed by the 14th of June. Until then, I
won't be accepting any PRs for Algebra II content.***

### Reading

These are as set of lectures notes from the second year mathematics bachelor
course "Algebra" at the ETH Zürich, read by Prof. R. Pandharipande in the
Autumn/Summer semesters of 2018/2019.

The Alebra II lecture notes were written by myself with the help of fellow
students. Philipp Weder kindly submitted his excellent Algebra I summary to be
included in this project.

### Disclaimer

I cannot guarantee completeness, nor correctness either due to time constraints
or knowledge gaps. These notes are mainly for my own understanding and a way to
share them along with the source in a practical way. In other words, please
don't rely on these being a complete and thorough script of the lectures,
instead see them as what they are: a set of non-exhaustive lecture notes.

Do not hesitate to send me an email if you discover any mistakes, ranging over
mathematical errors, spelling mistakes, grammatical or typesetting issues - I'm
open to any suggestions.

### Compiling yourself

TL;DR:

`latexmk -pdf *tex && latexmk -c`

Previously, these notes were written in Markdown. As the code became more
complex, this became more difficult to maintain. Now they are written in LaTeX.
To compile the notes yourself, make sure you have LaTeX installed. Then call

`latexmk -pdf *tex`

in the base directory of this project. To clean up all auxiliary files except
the `pdf` after compilation, call

`latexmk -c`

Most distributions of LaTeX will have this compilation tool by default.
