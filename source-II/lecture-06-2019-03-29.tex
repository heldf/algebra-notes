\setcounter{chapter}{5}
\chapter{Solvability}

In this lecture we will investigate a very specific field extension, namely that
of the rational functions on $n$ variables $\C(X_1, \ldots, X_n)$ over the field
of rational functions in the elementary symmetric functions $\C(e_1, \ldots,
e_n)$. We have seen elementary symmetric functions before in some examples, but
now we will finally define them in general.

\section{Elementary Symmetric Functions}

\begin{definition}[Elementary Symmetric Functions]
  Let $X_1, \ldots, X_n$ be variables. The set of \textbf{elementary symmetric
  functions} $\set{e_0, \ldots, e_n }$ on these $n$ variables is defined as the
  coefficients of the polynomial
  %
  \begin{align*}
    \prod _{k=1} ^n (Z - X_k) = \sum _{k=0} ^n e_{n-k} Z^k
  \end{align*}
  %
  we can also write them in a more concrete closed form:
  %
  \begin{gather*}
    \begin{cases}
      e_0 = 1                                       \\
      e_1 = \sum _{i=1} ^n X_i                      \\
      e_2 = \sum _{1 \leq i<j \leq n} X_i X_j       \\
      e_3 = \sum _{1 \leq i<j<k \leq n} X_i X_j X_k \\
      \cdots                                        \\
      e_n = \prod _{i=1} ^n X_i                     \\
    \end{cases}
  \end{gather*}
  %
  in general we have
  %
  \begin{align*}
    e_k =
    \begin{cases}
    1                                            & k=0 \\
    \sum _{1 \leq i_1 < \cdots < i_k \leq n}
      \left ( \prod _{l=1} ^{k} X_{i_l} \right ) & \text{else} \\
    \end{cases}
  \end{align*}
\end{definition}

\begin{remark}
  Note the strict inequalities between the inner indexes. The $X_i$'s are
  distinct. This gives rise to a definition in words: ``the $k$th symmetric
  elementary function is the sum of all distinct products of $k$ variables''
\end{remark}

% @todo
% Is this true?
% \begin{remark}
%   Strictly speaking, the elementary symmetric functions are elementary symmetric
%   \textbf{polynomials}.
% \end{remark}

\begin{example}[Elementary symmetric Functions on 3 variables]
  \begin{gather*}
    \begin{cases}
      e_0 = 1            \\
      e_1 = X + Y + Z    \\
      e_2 = XY + XZ + YZ \\
      e_3 = XYZ
    \end{cases}
  \end{gather*}
\end{example}

Their name stems from the fact, that owing to the nature of the product form
$\prod _{k=1} ^n (Z-X_k)$, the $e_k$'s are invariant under permutation of the
variables. That is $e_k (X_1, \ldots, X_n) = e_k (X_{\sigma(1)}, \ldots,
X_{\sigma(n)})$ for $\sigma \in S_n$.

\section{The extension over Elementary Symmetric Functions}

We can now go back to our original example, and ask ourselves is $\C(X_1,
\ldots, X_n) / \C(e_1, \ldots, e_n)$ a finite extesnion? If so, is it Galois? We
will find that the answer to both of these questions is yes. Define $E = \C(X_1,
\ldots, X_n), F = \C(e_1, \ldots, e_n)$ to prove this result.

\begin{proof}(The extension is finite)
  We claim that $E$ is the splitting field over $F$ of the polynomial
  $p(Z)~=~\prod _{j=1} ^n (Z-X_j)$. It surely, by construction, contains all the
  roots of $p$. We must now show that $p$ does not factorise into linear factors
  in any other subfield of $E$ @todo. In particular, as $E$ is the splitting
  field over $F$ of some polynomial with distinct roots, the extension is
  normal, and thus Galois.

  Now that we know that the extension is finite, we can continue to find the
  exact degree. From Algebra I, we know that
  \begin{align*}
    \mathrm{dim}(E/F) \leq n!
  \end{align*}

  \begin{remark}
     The proof for this statement lies in the fact that $E$ is a splitting
     field, meaning that it is no more than the base field with the roots
     adjoined. When we adjoin the first root $r_1$, the maximal degree of the
     extension is $n$, if $p$ is irreducible. Then we may factor $p/(Z-r_1) \in
     F(r_1)$ and continue adjoining the second root $r_2$. Again we may be
     dealing with an irreducible polynomial (in $F(r_1)$), thus the degree of
     the next extension is maxiamlly $n-1$. If we continue this process
     iteratively, we obtain $n!$ as an upper bound of the extension $E/F$.
  \end{remark}

  If we now look at the action of $S_n$ on $\{X_1, \ldots, X_n \}$, with $\sigma
  \in S_n$ acting by $\sigma(X_i) = X_{\sigma (i)}$ we see that $\sigma(e_k) =
  e_k$, $S_n$ poses a good candidate for the automorphism group as a group
  action. In particular, we know that the $e_k$'s form a basis of $F$, thus we
  *know* that $S_n$ fixes $F$, and hence must be a subset of the automorphism
  group. Of course however $|S_n| = n!$ and thus $S_n = \Aut(E/F)$.  Finally we
  have
  %
  \begin{gather*}
    n! \leq |\mathrm{Aut}(E/F)| \overset{\text{Gal.}}{=} \dim(E/F) \leq n! \\
    \implies \mathrm{dim}(E/F) = n!
  \end{gather*}
\end{proof}

\section{The subgroups of the extension}

Now that we know that $E/F$ is Galois with dimension $n!$ we can apply theorems
from the last lecture. Specifically, to study the subextensions of $E/F$, we can
look at the subgroups of the Galois group (Part 1 of F.T.G.T).

\begin{figure}[!h]
  \centering
  \begin{tikzpicture}
    \matrix(m)[%
      matrix of math nodes,
      row sep=0.25em,
      column sep=4em,
      minimum width=1em
    ]{
      E =\C(X_1, \ldots, X_n)  & \Gal(E/E) = \set{\id}                  \\
                               & \rotatebox[origin=c]{-90}{$\subseteq$} \\
      K                        & \Gal(E/K) = G                          \\
                               & \rotatebox[origin=c]{-90}{$\subseteq$} \\
      F = \C(e_1, \ldots, e_n) & \Gal(E/F) = S_n                        \\
    };
    \path[-stealth]
      (m-1-1) edge [-] (m-3-1)
      (m-3-1) edge [-] (m-5-1)

      (m-1-1) edge [draw,-,decorate,decoration={snake}]
              node [below] {\tiny corresponds} (m-1-2)
      (m-3-1) edge [draw,-,decorate,decoration={snake}]
              node [below] {\tiny corresponds} (m-3-2)
      (m-5-1) edge [draw,-,decorate,decoration={snake}]
              node [below] {\tiny corresponds} (m-5-2)
      ;
  \end{tikzpicture}
\end{figure}

\noindent
Before we do this in general, we can look at following

\begin{example}
  We know from group theory that
  %
  \begin{align*}
    \set{\id}
    \triangleleft \Z_2
    \triangleleft A_n
    \triangleleft K_4
    \triangleleft S_n
  \end{align*}
  %
  with
  %
  \begin{align*}
    \Z_2 & = \set{\id, (12)(34)}                     \\
    A_4  & = \set{\id, (12)(34), (13)(24), (14)(23)} \\
    K_4  & = \set{\id, (12)(34), (13)(24), (14)(23)}
  \end{align*}
\end{example}
